﻿using System.Net;
using System.Net.Http.Json;
using ChannelEngineAssessment.Api.Tests.Models;
using FluentAssertions;

namespace ChannelEngineAssessment.Api.Tests.Steps;

[Binding]
public class ProductManagementSteps
{
    private readonly HttpClient _httpClient; 
    private readonly ScenarioContext _scenarioContext;

    public ProductManagementSteps(HttpClient httpClient, ScenarioContext scenarioContext)
    {
        _httpClient = httpClient;
        _scenarioContext = scenarioContext;
    }

    [When(@"get top (.*) products in the system")]
    public async Task WhenGetTopProductsInTheSystem(int p0)
    {
        var response = await _httpClient.GetFromJsonAsync<ListResultViewModel<TopSoldProductViewModel>>("products/top-sold");
        _scenarioContext.Add("TopSoldProductsResponse", response);
    }
    
    [Then(@"products are returned successfully")]
    public void ThenProductsAreReturnedSuccessfully()
    {
        var response = _scenarioContext.Get<ListResultViewModel<TopSoldProductViewModel>>("TopSoldProductsResponse");
        response.Items.Should().HaveCount(5);
    }

    [Given(@"get top (.*) products in the system")]
    public async Task GivenGetTopProductsInTheSystem(int p0)
    {
        var response = await _httpClient.GetFromJsonAsync<ListResultViewModel<TopSoldProductViewModel>>("products/top-sold");
        _scenarioContext.Add("TopSoldProductsResponse", response);
    }

    [When(@"set stock for random product")]
    public async Task WhenSetStockForRandomProduct()
    {
        var topSoldProductsResponse = _scenarioContext.Get<ListResultViewModel<TopSoldProductViewModel>>("TopSoldProductsResponse");
        Random r = new Random();
        var productIndex = r.Next(0, topSoldProductsResponse.Items.Count());

        var productToSetStock = topSoldProductsResponse.Items.ToArray()[productIndex];

        var setProductStockViewModel = new SetProductStockViewModel
        {
            MerchantProductNo = productToSetStock.MerchantProductNo,
            Stock = 55
        };

        var content = JsonContent.Create(setProductStockViewModel);

        var response = await _httpClient.PatchAsync("products/set-stock", content);
        _scenarioContext.Add("SetStockResponse", response);
    }

    [Then(@"the product stock is set correctly")]
    public void ThenTheProductStockIsSetCorrectly()
    {
        var response = _scenarioContext.Get<HttpResponseMessage>("SetStockResponse");
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
}