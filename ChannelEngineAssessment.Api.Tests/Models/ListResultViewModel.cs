namespace ChannelEngineAssessment.Api.Tests.Models;

public class ListResultViewModel<TViewModel>
{
    public IEnumerable<TViewModel> Items { get; set; }
}