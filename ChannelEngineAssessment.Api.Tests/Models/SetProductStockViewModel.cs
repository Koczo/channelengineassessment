﻿namespace ChannelEngineAssessment.Api.Tests.Models;

public class SetProductStockViewModel
{
    public string MerchantProductNo { get; set; }
    public int Stock { get; set; }
}