namespace ChannelEngineAssessment.Api.Tests.Models;

public class TopSoldProductViewModel
{
    public string MerchantProductNo { get; set; }
    public string Name { get; set; }
    public string EAN { get; set; }
    public int TotalQuantity { get; set; }
}