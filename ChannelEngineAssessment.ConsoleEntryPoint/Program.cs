﻿using System;
using Autofac;
using Microsoft.Extensions.Configuration;
using ChannelEngineAssessment.Common.Configurations;
using ChannelEngineAssessment.ConsoleEntryPoint.Views;
using ChannelEngineAssessment.Common.IoC;
using ChannelEngineAssessment.Application.Products.Services;
using ChannelEngineAssessment.Infrastructure.Orders.Repositories;
using ChannelEngineAssessment.Domain.Orders.Factories;
using ChannelEngineAssessment.Application.Products.Handlers;

namespace ChannelEngineAssessment.ConsoleEntryPoint
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var container = ConfigureContainer();

            Terminal.Gui.Application.Init();
            Terminal.Gui.Application.Run(container.Resolve<MainView>());
            Terminal.Gui.Application.Shutdown();
        }

        public static IContainer ConfigureContainer()
        {
            var containerBuilder = new ContainerBuilder();
            IConfiguration Configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();

            var channelEngineEndpointsConfiguration = new ChannelEngineEndpointsConfiguration();
            Configuration.GetSection("ApplicationConfiguration:ChannelEngineEndpoints").Bind(channelEngineEndpointsConfiguration);
            containerBuilder.RegisterModule(new ConfigurationModule(
                new ApplicationConfiguration(
                    Configuration["ApplicationConfiguration:BaseApiUrl"],
                    Convert.ToBoolean(Configuration["ApplicationConfiguration:ThrowOnApiError"]),
                    Convert.ToBoolean(Configuration["ApplicationConfiguration:FailOnApiDeserializationError"]),
                    Configuration["ApplicationConfiguration:ApiKeyNameUrlParameter"],
                    Configuration["ApplicationConfiguration:ApiKey"],
                    channelEngineEndpoints: channelEngineEndpointsConfiguration)));

            containerBuilder.RegisterModule(new ApplicationModule<ProductService>());
            containerBuilder.RegisterModule(new RepositoriesModule<OrderRepository>());
            containerBuilder.RegisterModule(new FactoriesModule<OrderDomainFactory>());
            containerBuilder.RegisterModule(new MediatorModule(typeof(GetTopSoldProductsQueryHandler).Assembly));
            containerBuilder.RegisterModule(new AutoMapperModule(typeof(ProductsControllerMappingProfile).Assembly, typeof(OrderRepository).Assembly, typeof(MainView).Assembly));

            containerBuilder.RegisterType<MainView>().AsSelf().InstancePerLifetimeScope();

            return containerBuilder.Build();
        }
    }
}
