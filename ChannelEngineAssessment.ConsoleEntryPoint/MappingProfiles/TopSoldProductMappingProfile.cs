﻿using AutoMapper;
using ChannelEngineAssessment.ConsoleEntryPoint.Models;
using ChannelEngineAssessment.Infrastructure.Products.Dtos;

namespace ChannelEngineAssessment.ConsoleEntryPoint.MappingProfiles
{
    public class TopSoldProductMappingProfile : Profile
    {
        public TopSoldProductMappingProfile()
        {
            CreateMap<TopSoldProductDto, TopSoldProduct>();
        }
    }
}
