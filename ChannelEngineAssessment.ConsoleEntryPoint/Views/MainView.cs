﻿
using AutoMapper;
using ChannelEngineAssessment.ConsoleEntryPoint.Models;
using ChannelEngineAssessment.Infrastructure.Products.Commands;
using ChannelEngineAssessment.Infrastructure.Products.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terminal.Gui;

namespace ChannelEngineAssessment.ConsoleEntryPoint.Views
{
    public class MainView : Window
    {
        ListView ListView { get; set; }
        private IEnumerable<TopSoldProduct> _topSoldProducts = new List<TopSoldProduct>();
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public MainView(IMediator mediator, IMapper mapper)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));

            InitializeView();
        }

        private async void InitializeView()
        {
            await InitializeDataAsync();
            await InitializeControlsAsync();
        }

        private async Task InitializeControlsAsync()
        {
            var topProductsFrame = InitializeTopProductsFrameView();
            var topProductsListView = await InitializeTopProductsListView(topProductsFrame);
            var setStockButton = SetStockButton(topProductsListView, topProductsFrame);
        }

        private FrameView InitializeTopProductsFrameView()
        {
            return new FrameView("Top sold products")
            {
                X = 0,
                Y = 0,
                Width = Dim.Fill(),
                Height = Dim.Percent(80),
            };
        }
        private async Task<ListView> InitializeTopProductsListView(View previous)
        {
            ListView = new ListView()
            {
                X = 0,
                Y = 0,
                Width = Dim.Fill(),
                Height = Dim.Fill(),
            };

            await ListView.SetSourceAsync(_topSoldProducts.ToList());
            previous.Add(ListView);

            return ListView;
        }

        private Button SetStockButton(View previous, View listFrame)
        {
            var setStockButton = new Button("Set stock to 25")
            {
                X = 0,
                Y = Pos.Top(listFrame) + 10,
                Width = 40
            };
            setStockButton.Clicked += async () => await ChangeSelectedProductStockAsync();
            Add(listFrame, setStockButton);
            return setStockButton;
        }

        private async  Task ChangeSelectedProductStockAsync()
        {
            var selectedProduct = _topSoldProducts.ToArray()[ListView.SelectedItem];
            var command = new SetProductStockCommand(selectedProduct.MerchantProductNo, 25);

            await _mediator.Send(command);
            await InitializeDataAsync();
        }

        private async Task<IEnumerable<TopSoldProduct>> GetTopSoldProductsAsync()
        {
            var query = new GetTopSoldProductsQuery();

            var result = await _mediator.Send(query);

            return _mapper.Map<IEnumerable<TopSoldProduct>>(result.Items);
        }

        private async Task InitializeDataAsync()
        {
            _topSoldProducts = await GetTopSoldProductsAsync();
        }
    }
}
