﻿using ChannelEngineAssessment.Infrastructure.Products.Dtos;

namespace ChannelEngineAssessment.ConsoleEntryPoint.Models
{
    public class TopSoldProduct : TopSoldProductDto
    {
        public override string ToString()
        {
            return $"No.: {MerchantProductNo.Id} | Name: {Name} | EAN: {EAN} | Total quantity: {TotalQuantity}";
        }
    }
}
