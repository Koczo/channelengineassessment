﻿using ChannelEngineAssessment.Common.Dtos;

namespace ChannelEngineAssessment.Infrastructure.Orders.Dtos
{
    public class OrderResponseListDto : ListResponseDto<OrderResponseDto>
    {
    }
}
