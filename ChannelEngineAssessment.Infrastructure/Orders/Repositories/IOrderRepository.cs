﻿using ChannelEngineAssessment.Common.Infrastructure.Repositories;
using ChannelEngineAssessment.Domain.Orders.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChannelEngineAssessment.Infrastructure.Orders.Repositories
{
    public interface IOrderRepository : IGenericRepository<Order>
    {
        Task<IEnumerable<Order>> GetByStatusesAsync(IEnumerable<OrderStatus> statuses);
    }
}
