﻿using AutoMapper;
using ChannelEngineAssessment.Common.Configurations;
using ChannelEngineAssessment.Common.Domain;
using ChannelEngineAssessment.Common.Infrastructure.Repositories;
using ChannelEngineAssessment.Domain.Orders.Model;
using ChannelEngineAssessment.Infrastructure.Orders.Dtos;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelEngineAssessment.Infrastructure.Orders.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly IGenericRestRepository _genericRestRepository;
        private readonly IApplicationConfiguration _applicationConfiguration;
        private readonly IMapper _mapper;

        public OrderRepository(IGenericRestRepository genericRestRepository, IApplicationConfiguration applicationConfiguration, IMapper mapper)
        {
            _genericRestRepository = genericRestRepository ?? throw new ArgumentNullException(nameof(genericRestRepository));
            _applicationConfiguration = applicationConfiguration ?? throw new ArgumentNullException(nameof(applicationConfiguration));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<Order>> GetAllAsync()
        {
            return await GetByFilter();
        }

        public Task<Order> GetAsync(AggregateId id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Order>> GetByStatusesAsync(IEnumerable<OrderStatus> statuses)
        {
            return await GetByFilter(statuses);
        }

        private async Task<IEnumerable<Order>> GetByFilter(IEnumerable<OrderStatus> statuses = null, IEnumerable<string> emailAddresses = null,
            DateTime? fromDate = null, DateTime? toDate = null, DateTime? fromCreatedAtDate = null, DateTime? toCreatedAtDate = null,
            bool? excludeMarketplaceFulfilledOrdersAndLines = null, string fulfillmentType = null,
            IEnumerable<int> channelIds = null, IEnumerable<int> stockLocationIds = null, int? page = null)
        {
            var restRequest = new RestRequest(_applicationConfiguration.ChannelEngineEndpoints.GetOrdersEndpoint);
            ApplyFilters(restRequest, statuses, emailAddresses, fromDate, toDate, fromCreatedAtDate, toCreatedAtDate,
                excludeMarketplaceFulfilledOrdersAndLines, fulfillmentType, channelIds,
                stockLocationIds, page);

            var orders = await _genericRestRepository.GetAsync<OrderResponseListDto>(restRequest);
            return _mapper.Map<IEnumerable<Order>>(orders.Content);
        }

        private void ApplyFilters(RestRequest restRequest, IEnumerable<OrderStatus> statuses, IEnumerable<string> emailAddresses,
            DateTime? fromDate, DateTime? toDate, DateTime? fromCreatedAtDate, DateTime? toCreatedAtDate,
            bool? excludeMarketplaceFulfilledOrdersAndLines, string fulfillmentType, IEnumerable<int> channelIds, IEnumerable<int> stockLocationIds, int? page)
        {
            if (statuses != null && statuses.Any())
            {
                foreach (var orderStatus in statuses)
                {
                    restRequest.AddParameter(nameof(statuses), orderStatus.DisplayName, ParameterType.QueryString);
                }
            }

            if (emailAddresses != null && emailAddresses.Any())
            {
                foreach (var emailAddress in emailAddresses)
                {
                    restRequest.AddParameter(nameof(emailAddresses), emailAddress, ParameterType.QueryString);
                }
            }

            if (fromDate.HasValue)
            {
                restRequest.AddParameter(nameof(fromDate), fromDate, ParameterType.QueryString);
            }

            if (toDate.HasValue)
            {
                restRequest.AddParameter(nameof(toDate), toDate, ParameterType.QueryString);
            }

            if (fromCreatedAtDate.HasValue)
            {
                restRequest.AddParameter(nameof(fromCreatedAtDate), fromCreatedAtDate, ParameterType.QueryString);
            }

            if (toCreatedAtDate.HasValue)
            {
                restRequest.AddParameter(nameof(fromCreatedAtDate), toCreatedAtDate, ParameterType.QueryString);
            }

            if (excludeMarketplaceFulfilledOrdersAndLines.HasValue)
            {
                restRequest.AddParameter(nameof(excludeMarketplaceFulfilledOrdersAndLines), excludeMarketplaceFulfilledOrdersAndLines, ParameterType.QueryString);
            }

            if (!string.IsNullOrEmpty(fulfillmentType))
            {
                restRequest.AddParameter(nameof(fulfillmentType), fulfillmentType, ParameterType.QueryString);
            }

            if (channelIds != null && channelIds.Any())
            {
                foreach (var channelId in channelIds)
                {
                    restRequest.AddParameter(nameof(channelIds), channelId, ParameterType.QueryString);
                }
            }

            if (stockLocationIds != null && stockLocationIds.Any())
            {
                foreach (var stockLocationId in stockLocationIds)
                {
                    restRequest.AddParameter(nameof(stockLocationIds), stockLocationId, ParameterType.QueryString);
                }
            }

            if (page.HasValue)
            {
                restRequest.AddParameter(nameof(page), page, ParameterType.QueryString);
            }
        }

    }
}
