﻿using AutoMapper;
using ChannelEngineAssessment.Common.Domain;
using ChannelEngineAssessment.Domain;
using ChannelEngineAssessment.Domain.Orders.Model;
using ChannelEngineAssessment.Infrastructure.Orders.Dtos;

namespace ChannelEngineAssessment.Infrastructure.Orders
{
    public class OrderMappingProfile : Profile
    {
        public OrderMappingProfile()
        {
            CreateMap<OrderResponseDto, Order>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => new AggregateId(x.ChannelOrderNo)))
                .ForMember(x => x.ChannelOrderSupport, opt => opt.MapFrom(x => Enumeration.FromValue<OrderSupport>((int)x.ChannelOrderSupport)))
                .ForMember(x => x.Status, opt => opt.MapFrom(x => Enumeration.FromValue<OrderStatus>((int)x.Status)))
                .ForMember(x => x.OriginalShippingPriceWithVat,
                    opt => opt.MapFrom(x => new PriceWithVat(x.OriginalShippingCostsVat, x.OriginalShippingCostsInclVat)))
                .ForMember(x => x.OriginalSubtotalPriceWithVat,
                    opt => opt.MapFrom(x => new PriceWithVat(x.OriginalSubTotalVat ?? 0m, x.OriginalSubTotalVat ?? 0m)))
                .ForMember(x => x.OriginalTotalPriceWithVat,
                    opt => opt.MapFrom(x => new PriceWithVat(x.OriginalTotalVat ?? 0m, x.OriginalTotalInclVat ?? 0m)))
                .ForMember(x => x.ShippingPriceWithVat,
                    opt => opt.MapFrom(x => new PriceWithVat(x.ShippingCostsVat ?? 0m, x.ShippingCostsInclVat ?? 0m)))
                .ForMember(x => x.SubTotalPriceWithVat,
                    opt => opt.MapFrom(x => new PriceWithVat(x.SubTotalVat ?? 0m, x.OriginalSubTotalVat ?? 0m)))
                .ForMember(x => x.TotalPriceWithVat,
                    opt => opt.MapFrom(x => new PriceWithVat(x.TotalVat ?? 0m, x.TotalInclVat ?? 0m)))
                .ForMember(x => x.BillingAddress, opt => opt.MapFrom(x => new Address(x.BillingAddress.Line1, x.BillingAddress.Line2, x.BillingAddress.Line3,
                Enumeration.FromValue<Domain.Gender>((int)x.BillingAddress.Gender), x.BillingAddress.CompanyName, x.BillingAddress.FirstName,
                    x.BillingAddress.LastName, x.BillingAddress.StreetName, x.BillingAddress.HouseNr, x.BillingAddress.HouseNrAddition, x.BillingAddress.ZipCode,
                    x.BillingAddress.City, x.BillingAddress.Region, x.BillingAddress.CountryIso, x.BillingAddress.Original)))
                .ForMember(x => x.ShippingAddress, opt => opt.MapFrom(x => new Address(x.ShippingAddress.Line1, x.ShippingAddress.Line2, x.ShippingAddress.Line3,
                Enumeration.FromValue<Domain.Gender>((int)x.ShippingAddress.Gender), x.ShippingAddress.CompanyName, x.ShippingAddress.FirstName,
                    x.ShippingAddress.LastName, x.ShippingAddress.StreetName, x.ShippingAddress.HouseNr, x.ShippingAddress.HouseNrAddition, x.ShippingAddress.ZipCode,
                    x.ShippingAddress.City, x.ShippingAddress.Region, x.ShippingAddress.CountryIso, x.ShippingAddress.Original)));

            CreateMap<OrderLineResponseDto, OrderLine>()
                .ForMember(x => x.Status, opt => opt.MapFrom(x => Enumeration.FromValue<OrderStatus>((int)x.Status)))
                .ForMember(x => x.Condition, opt => opt.MapFrom(x => Enumeration.FromValue<OrderLineCondition>((int)x.Condition)))
                .ForMember(x => x.LineTotalPriceWithVat,
                    opt => opt.MapFrom(x => new PriceWithVat(x.LineVat ?? 0m, x.LineTotalInclVat ?? 0m)))
                .ForMember(x => x.OriginalLineTotalPriceWithVat,
                    opt => opt.MapFrom(x => new PriceWithVat(x.OriginalLineVat ?? 0m, x.OriginalLineTotalInclVat ?? 0m)))
                .ForMember(x => x.OriginalUnitPriceWithVat,
                    opt => opt.MapFrom(x => new PriceWithVat(x.OriginalUnitVat ?? 0m, x.OriginalUnitPriceInclVat ?? 0m)));

            CreateMap<AddressDto, Address>()
                .ConstructUsing(x => new Address(x.Line1, x.Line2, x.Line3, Enumeration.FromValue<Domain.Gender>((int)x.Gender), x.CompanyName, x.FirstName,
                    x.LastName, x.StreetName, x.HouseNr, x.HouseNrAddition, x.ZipCode, x.City, x.Region, x.CountryIso,
                    x.Original));

            CreateMap<string, AggregateId>().ConstructUsing(x => new AggregateId(x));

        }
    }
}