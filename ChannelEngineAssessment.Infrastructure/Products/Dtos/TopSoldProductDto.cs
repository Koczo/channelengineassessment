﻿using ChannelEngineAssessment.Common.Domain;

namespace ChannelEngineAssessment.Infrastructure.Products.Dtos
{
    public class TopSoldProductDto
    {
        public AggregateId MerchantProductNo { get; set; }
        public string Name { get; set; }
        public string EAN { get; set; }
        public int TotalQuantity { get; set; }
    }
}
