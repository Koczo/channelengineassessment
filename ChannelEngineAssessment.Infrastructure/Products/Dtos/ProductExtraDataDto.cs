﻿using ChannelEngineAssessment.Common.Domain.Products.Enums;

namespace ChannelEngineAssessment.Infrastructure.Products.Dtos
{
    public class ProductExtraDataDto
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public ProductExtraDataType Type { get; set; }
        public bool IsPublic { get; set; }
    }
}
