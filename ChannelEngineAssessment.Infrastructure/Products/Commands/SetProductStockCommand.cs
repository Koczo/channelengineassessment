﻿using ChannelEngineAssessment.Common.Domain;
using MediatR;

namespace ChannelEngineAssessment.Infrastructure.Products.Commands
{
    public class SetProductStockCommand : IRequest
    {
        public AggregateId MerchantProductNo { get; }
        public int Stock { get; }

        public SetProductStockCommand(AggregateId merchantProductNo, int stock)
        {
            MerchantProductNo = merchantProductNo;
            Stock = stock;
        }
    }
}
