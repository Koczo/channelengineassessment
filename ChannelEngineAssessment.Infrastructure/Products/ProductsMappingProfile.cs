﻿using AutoMapper;
using ChannelEngineAssessment.Common.Domain;
using ChannelEngineAssessment.Domain.Products.Model;
using ChannelEngineAssessment.Infrastructure.Products.Dtos;
using ChannelEngineAssessment.Infrastructure.Products.Requests;
using System.Collections.Generic;
using System.Linq;

namespace ChannelEngineAssessment.Infrastructure.Products
{
    public class ProductsMappingProfile : Profile
    {
        public ProductsMappingProfile()
        {
            CreateMap<ProductResponseDto, Product>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => new AggregateId(x.MerchantProductNo)))
                .ForMember(x => x.VatRateType, opt => opt.MapFrom(x => Enumeration.FromValue<VatRateType>((int)x.VatRateType)))
                .ForMember(x => x.ExtraImageUrls, opt => opt.MapFrom(
                    u =>
                        new List<ExtraImageUrl>()
                        {
                            new ExtraImageUrl(1, u.ExtraImageUrl1),
                            new ExtraImageUrl(2, u.ExtraImageUrl2),
                            new ExtraImageUrl(3, u.ExtraImageUrl3),
                            new ExtraImageUrl(4, u.ExtraImageUrl4),
                            new ExtraImageUrl(5, u.ExtraImageUrl5),
                            new ExtraImageUrl(6, u.ExtraImageUrl6),
                            new ExtraImageUrl(7, u.ExtraImageUrl7),
                            new ExtraImageUrl(8, u.ExtraImageUrl8),
                            new ExtraImageUrl(9, u.ExtraImageUrl9),
                        }))
                .ForMember(x => x.ExtraData,
                    opt => opt.MapFrom(x =>
                        x.ExtraData.Select(d => new ProductExtraData(d.Key, d.Value, Enumeration.FromValue<ProductExtraDataType>((int)d.Type), d.IsPublic))));

            CreateMap<Product, UpsertProductRequest>()
                .ConstructUsing(aggregate =>
                    new UpsertProductRequest(
                    aggregate.Id.Id,
                    aggregate.Name,
                    aggregate.Description,
                    aggregate.Brand,
                    aggregate.Size,
                    aggregate.Color,
                    aggregate.Ean,
                    aggregate.ManufacturerProductNumber,
                    aggregate.Stock,
                    aggregate.Price,
                    aggregate.MSRP,
                    aggregate.PurchasePrice,
                    aggregate.VatRateType.DisplayName,
                    aggregate.ShippingCost,
                    aggregate.ShippingTime,
                    aggregate.Url,
                    aggregate.ImageUrl,
                    aggregate.ExtraImageUrls.FirstOrDefault() != null ? aggregate.ExtraImageUrls.FirstOrDefault().Url : null,
                    aggregate.ExtraImageUrls.Skip(1).FirstOrDefault() != null ? aggregate.ExtraImageUrls.Skip(1).FirstOrDefault().Url : null,
                    aggregate.ExtraImageUrls.Skip(2).FirstOrDefault() != null ? aggregate.ExtraImageUrls.Skip(2).FirstOrDefault().Url : null,
                    aggregate.ExtraImageUrls.Skip(3).FirstOrDefault() != null ? aggregate.ExtraImageUrls.Skip(3).FirstOrDefault().Url : null,
                    aggregate.ExtraImageUrls.Skip(4).FirstOrDefault() != null ? aggregate.ExtraImageUrls.Skip(4).FirstOrDefault().Url : null,
                    aggregate.ExtraImageUrls.Skip(5).FirstOrDefault() != null ? aggregate.ExtraImageUrls.Skip(5).FirstOrDefault().Url : null,
                    aggregate.ExtraImageUrls.Skip(6).FirstOrDefault() != null ? aggregate.ExtraImageUrls.Skip(6).FirstOrDefault().Url : null,
                    aggregate.ExtraImageUrls.Skip(7).FirstOrDefault() != null ? aggregate.ExtraImageUrls.Skip(7).FirstOrDefault().Url : null,
                    aggregate.ExtraImageUrls.Skip(8).FirstOrDefault() != null ? aggregate.ExtraImageUrls.Skip(8).FirstOrDefault().Url : null,
                    aggregate.CategoryTrail,
                    aggregate.ExtraData.Select(x => new ProductExtraDataRequest(x.Key, x.Value, x.Type.DisplayName, x.IsPublic)).ToList()));

            CreateMap<ProductExtraData, ProductExtraDataRequest>()
                .ConstructUsing(x => new ProductExtraDataRequest(x.Key, x.Value, x.Type.DisplayName, x.IsPublic));
        }

    }
}
