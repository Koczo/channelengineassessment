﻿
using ChannelEngineAssessment.Common.Domain.Orders.Enums;
using ChannelEngineAssessment.Common.Dtos;
using ChannelEngineAssessment.Infrastructure.Products.Dtos;
using MediatR;
using System.Collections.Generic;

namespace ChannelEngineAssessment.Infrastructure.Products.Queries
{
    public class GetTopSoldProductsQuery : IRequest<ListResultDto<TopSoldProductDto>>
    {
        public int NumberOfProducts { get; }
        public IEnumerable<OrderStatus> OrderStatuses { get; }
        public GetTopSoldProductsQuery(int numberOfProducts = 5)
        {
            NumberOfProducts = numberOfProducts;
            OrderStatuses = new List<OrderStatus>() { OrderStatus.InProgress };
        }
    }
}
