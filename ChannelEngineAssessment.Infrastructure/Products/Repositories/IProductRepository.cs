﻿using ChannelEngineAssessment.Common.Infrastructure.Repositories;
using ChannelEngineAssessment.Domain.Products.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChannelEngineAssessment.Infrastructure.Products.Repositories
{
    public interface IProductRepository : IGenericRepository<Product>
    {
        Task CreateAsync(Product product);
        Task UpdateAsync(Product product);
        Task<IEnumerable<Product>> GetByMerchantProductNoAsync(IEnumerable<string> merchantProductNoList);
    }
}
