﻿using AutoMapper;
using ChannelEngineAssessment.Common.Configurations;
using ChannelEngineAssessment.Common.Domain;
using ChannelEngineAssessment.Common.Dtos;
using ChannelEngineAssessment.Common.Infrastructure.Repositories;
using ChannelEngineAssessment.Domain.Products.Model;
using ChannelEngineAssessment.Infrastructure.Products.Dtos;
using ChannelEngineAssessment.Infrastructure.Products.Requests;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelEngineAssessment.Infrastructure.Products.Repositories
{
    public class ProductsRepository : IProductRepository
    {
        private readonly IGenericRestRepository _genericRestRepository;
        private readonly IMapper _mapper;
        private readonly IApplicationConfiguration _applicationConfiguration;

        public ProductsRepository(IGenericRestRepository genericRestRepository, IMapper mapper, IApplicationConfiguration applicationConfiguration)
        {
            _genericRestRepository = genericRestRepository ?? throw new ArgumentNullException(nameof(genericRestRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _applicationConfiguration = applicationConfiguration ?? throw new ArgumentNullException(nameof(applicationConfiguration));
        }

        public async Task<IEnumerable<Product>> GetAllAsync()
        {
            return await GetByFilter();
        }

        public async Task<Product> GetAsync(AggregateId id)
        {
            var restRequest = new RestRequest(_applicationConfiguration.ChannelEngineEndpoints.GetProductEndpoint);
            restRequest.AddParameter(nameof(id), id, ParameterType.UrlSegment);

            var result = await _genericRestRepository.GetAsync<ResponseDto<ProductResponseDto>>(restRequest);
            return _mapper.Map<Product>(result.Content);

        }

        public async Task<IEnumerable<Product>> GetByMerchantProductNoAsync(IEnumerable<string> merchantProductNoList)
        {
            return await GetByFilter(merchantProductNoList);
        }

        public async Task CreateAsync(Product product)
        {
            var restRequest = new RestRequest(_applicationConfiguration.ChannelEngineEndpoints.UpsertProductsEndpoint);
            restRequest.AddJsonBody(new List<UpsertProductRequest>() { _mapper.Map<UpsertProductRequest>(product) });

            await _genericRestRepository.PostAsync<object>(restRequest);
        }

        public async Task UpdateAsync(Product product)
        {
            await CreateAsync(product);
        }

        private async Task<IEnumerable<Product>> GetByFilter(IEnumerable<string> merchantProductNoList = null)
        {
            var restRequest = new RestRequest(_applicationConfiguration.ChannelEngineEndpoints.GetProductsEndpoint);
            ApplyFilters(restRequest, merchantProductNoList);

            var result = await _genericRestRepository.GetAsync<ListResponseDto<ProductResponseDto>>(restRequest);
            return _mapper.Map<IEnumerable<Product>>(result.Content);
        }

        private void ApplyFilters(RestRequest restRequest, IEnumerable<string> merchantProductNoList)
        {
            if (merchantProductNoList != null && merchantProductNoList.Any())
            {
                foreach (var no in merchantProductNoList)
                {
                    restRequest.AddParameter(nameof(merchantProductNoList), no, ParameterType.QueryString);
                }
            }
        }
    }
}
