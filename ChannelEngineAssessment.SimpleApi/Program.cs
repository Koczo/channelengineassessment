using Autofac;
using Autofac.Extensions.DependencyInjection;
using ChannelEngineAssessment.Application.Products.Handlers;
using ChannelEngineAssessment.Application.Products.Services;
using ChannelEngineAssessment.Common.Configurations;
using ChannelEngineAssessment.Common.EndpointDefinitions;
using ChannelEngineAssessment.Common.IoC;
using ChannelEngineAssessment.Domain.Orders.Factories;
using ChannelEngineAssessment.Infrastructure.Orders.Repositories;
using ChannelEngineAssessment.SimpleApi.EndpointDefinitions;
using ChannelEngineAssessment.Web.ViewModels.MappingProfiles;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddEndpontDefinitions(typeof(ProductEndpointDefinition));

builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory());

builder.Host.ConfigureContainer<ContainerBuilder>(builder => 
{
    IConfiguration Configuration = new ConfigurationBuilder()
        .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
        .Build();

    var channelEngineEndpointsConfiguration = new ChannelEngineEndpointsConfiguration();
    Configuration.GetSection("ApplicationConfiguration:ChannelEngineEndpoints").Bind(channelEngineEndpointsConfiguration);
    builder.RegisterModule(new ConfigurationModule(
        new ApplicationConfiguration(
            Configuration["ApplicationConfiguration:BaseApiUrl"],
            Convert.ToBoolean(Configuration["ApplicationConfiguration:ThrowOnApiError"]),
            Convert.ToBoolean(Configuration["ApplicationConfiguration:FailOnApiDeserializationError"]),
            Configuration["ApplicationConfiguration:ApiKeyNameUrlParameter"],
            Configuration["ApplicationConfiguration:ApiKey"],
            channelEngineEndpoints: channelEngineEndpointsConfiguration)));

    builder.RegisterModule(new ApplicationModule<ProductService>());
    builder.RegisterModule(new RepositoriesModule<OrderRepository>());
    builder.RegisterModule(new FactoriesModule<OrderDomainFactory>());
    builder.RegisterModule(new MediatorModule(typeof(GetTopSoldProductsQueryHandler).Assembly));
    builder.RegisterModule(new AutoMapperModule(typeof(ProductsControllerMappingProfile).Assembly, typeof(OrderRepository).Assembly));

});

var app = builder.Build();
app.UseEndpontDefinitions();

app.MapGet("/", () => "Hello World!");



app.Run();
