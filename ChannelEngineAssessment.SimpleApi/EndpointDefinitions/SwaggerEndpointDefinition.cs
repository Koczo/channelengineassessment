﻿using ChannelEngineAssessment.Common.EndpointDefinitions;
using Microsoft.OpenApi.Models;

namespace ChannelEngineAssessment.SimpleApi.EndpointDefinitions
{
    public partial class ProductEndpointDefinition
    {
        public class SwaggerEndpointDefinition : IEndpointDefinition
        {
            public void DefineEndpoints(WebApplication application)
            {
                application.UseSwagger();

                application.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "SimpleApi v1");

                    // To serve SwaggerUI at application's root page, set the RoutePrefix property to an empty string.
                    c.RoutePrefix = string.Empty;
                });
            }

            public void DefineServices(IServiceCollection services)
            {
                services.AddEndpointsApiExplorer();
                services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Simple Api", Version = "v1" });
                });

            }
        }
    }
}
