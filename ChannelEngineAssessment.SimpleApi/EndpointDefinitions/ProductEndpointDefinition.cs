﻿using ChannelEngineAssessment.Common.Domain;
using ChannelEngineAssessment.Common.EndpointDefinitions;
using ChannelEngineAssessment.Infrastructure.Products.Commands;
using ChannelEngineAssessment.Infrastructure.Products.Queries;
using MediatR;
using System.Reflection;

namespace ChannelEngineAssessment.SimpleApi.EndpointDefinitions
{
    public partial class ProductEndpointDefinition : IEndpointDefinition
    {
        public void DefineEndpoints(WebApplication application)
        {
            application.MapGet("/products/top-sold", GetTopSoldProductsAsync);
            application.MapPut("/products/set-stock", SetProductStockAsync);
        }

        public void DefineServices(IServiceCollection services)
        {
            //services.AddMediatR(Assembly.GetExecutingAssembly());

        }

        internal async Task<IResult> GetTopSoldProductsAsync(IMediator mediator)
        {
            var query = new GetTopSoldProductsQuery();

            var result = await mediator.Send(query);

            return result.Items.Any() ? Results.Ok(result.Items) : Results.NotFound();
        }

        internal async Task<IResult> SetProductStockAsync(IMediator mediator, string merchantProductNo, int stock)
        {
            var command = new SetProductStockCommand(new AggregateId(merchantProductNo), stock);

            await mediator.Send(command);

            return Results.Ok();
        }
    }
}
