﻿namespace ChannelEngineAssessment.Common.Domain
{
    public enum Gender
    {
        Male = 1,
        Female = 2,
        NotApplicable = 3
    }
}
