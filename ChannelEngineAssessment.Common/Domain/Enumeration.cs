﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ChannelEngineAssessment.Common.Domain
{
    public abstract partial class Enumeration : IComparable, IEquatable<Enumeration>
    {
        public int Value { get; }
        public string DisplayName { get; }

        protected Enumeration()
        {
        }

        protected Enumeration(int value, string displayName)
        {
            Value = value;
            DisplayName = displayName;
        }

        public override string ToString()
        {
            return DisplayName;
        }

        public static IEnumerable<T> GetAll<T>() where T : Enumeration =>
            from field in typeof(T).GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly)
            where field.FieldType == typeof(T)
            let value = (T)field.GetValue(null)
            where value != null
            select value;

        public bool Equals(Enumeration other)
        {
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Enumeration enumeration && Equals(enumeration);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public static int AbsoluteDifference(Enumeration firstValue, Enumeration secondValue)
        {
            var absoluteDifference = Math.Abs(firstValue.Value - secondValue.Value);
            return absoluteDifference;
        }

        public static T FromValue<T>(int value) where T : Enumeration
        {
            var matchingItem = Parse<T, int>(value, "value", item => item.Value == value);
            return matchingItem;
        }

        public static T FromValue<T>(int value, T enumerationDefault) where T : Enumeration =>
            IsCorrectValue<T>(value) ? FromValue<T>(value) : enumerationDefault;

        public static T FromDisplayName<T>(string displayName) where T : Enumeration
        {
            var matchingItem = Parse<T, string>(displayName, "display name", item => item.DisplayName == displayName);
            return matchingItem;
        }

        public static bool IsCorrectValue<T>(int value) where T : Enumeration =>
            TryParse(value, "value", item => item.Value == value, out T _);

        private static T Parse<T, K>(K value, string description, Func<T, bool> predicate) where T : Enumeration
        {
            if (!TryParse(value, description, predicate, out T result))
                throw new ApplicationException($"'{value}' is not a valid {description} in {typeof(T)}");

            return result;
        }
        private static bool TryParse<T, K>(K value, string description, Func<T, bool> predicate, out T result) where T : Enumeration
        {
            result = GetAll<T>().FirstOrDefault(predicate);
            return result != null;
        }

        public int CompareTo(Enumeration other)
        {
            return Value.CompareTo(other.Value);
        }

        public int CompareTo(Object obj)
        {
            if (obj == null)
            {
                return 1;
            }
            if (!(obj is Enumeration))
            {
                throw new ArgumentException("Object must be of type Enumeration");
            }

            return CompareTo((Enumeration)obj);
        }
    }
}
