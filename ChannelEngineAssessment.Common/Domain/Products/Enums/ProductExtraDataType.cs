﻿namespace ChannelEngineAssessment.Common.Domain.Products.Enums
{
    public enum ProductExtraDataType
    {
        Text = 1,
        Number = 2,
        Url = 3,
        ImageUrl = 4
    }
}
