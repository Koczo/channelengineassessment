﻿namespace ChannelEngineAssessment.Common.Domain.Products.Enums
{
    public enum VatRateType
    {
        None = 0,
        Standard = 1,
        Reduced = 2,
        SuperReduced = 3,
        Exempt = 4
    }
}
