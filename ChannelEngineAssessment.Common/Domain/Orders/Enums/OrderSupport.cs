﻿namespace ChannelEngineAssessment.Common.Domain.Orders.Enums
{
    public enum OrderSupport
    {
        None = 1,
        Orders = 2,
        SplitOrders = 3,
        SplitOrderLines = 4
    }
}
