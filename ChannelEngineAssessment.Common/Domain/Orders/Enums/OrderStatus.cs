﻿namespace ChannelEngineAssessment.Common.Domain.Orders.Enums
{
    public enum OrderStatus
    {
        New = 1,
        InProgress = 2,
        Shipped = 3,
        Manco = 4,
        InBackorder = 5,
        InCombi = 6,
        RequiresCorrection = 7,
        Returned = 8,
        Closed = 9,
        Canceled = 10,
    }
}
