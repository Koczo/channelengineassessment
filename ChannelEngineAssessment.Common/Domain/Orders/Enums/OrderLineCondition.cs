﻿namespace ChannelEngineAssessment.Common.Domain.Orders.Enums
{
    public enum OrderLineCondition
    {
        New,
        NewRefurbished,
        UsedAsNew,
        UsedGood,
        UsedReasonable,
        UsedMediocre,
        Unknown,
        UsedVeryGood
    }
}
