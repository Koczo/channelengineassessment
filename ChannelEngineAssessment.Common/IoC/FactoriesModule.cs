﻿using Autofac;
using ChannelEngineAssessment.Common.Domain;
using ChannelEngineAssessment.Common.Infrastructure.Factories;
using System.Linq;
using Module = Autofac.Module;

namespace ChannelEngineAssessment.Common.IoC
{
    public class FactoriesModule<TFactory> : Module where TFactory : IDomainFactory
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            var domainAssembly = typeof(TFactory).Assembly;

            builder.RegisterType<RestClientFactory>().As<IRestClientFactory>().InstancePerLifetimeScope();
            builder.RegisterAssemblyTypes(domainAssembly).Where(x => x.IsAssignableTo<IDomainFactory>()).AsImplementedInterfaces().InstancePerLifetimeScope();
        }
    }
}
