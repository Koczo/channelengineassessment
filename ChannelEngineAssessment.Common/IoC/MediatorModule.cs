﻿using Autofac;
using MediatR;
using System;
using System.Reflection;
using Module = Autofac.Module;

namespace ChannelEngineAssessment.Common.IoC
{
    public class MediatorModule : Module
    {
        private readonly Assembly _assembly;

        public MediatorModule(Assembly assembly)
        {
            _assembly = assembly ?? throw new ArgumentNullException(nameof(assembly));
        }

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            // Mediator itself
            builder
                .RegisterType<Mediator>()
                .As<IMediator>()
                .InstancePerLifetimeScope();

            // request & notification handlers
            builder.Register<ServiceFactory>(context =>
            {
                var c = context.Resolve<IComponentContext>();
                return t => c.Resolve(t);
            });

            builder.RegisterAssemblyTypes(_assembly).AsImplementedInterfaces(); // via assembly scan
        }
    }
}
