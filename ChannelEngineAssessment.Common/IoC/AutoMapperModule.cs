﻿using Autofac;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Module = Autofac.Module;
using TypeExtensions = Autofac.TypeExtensions;

namespace ChannelEngineAssessment.Common.IoC
{
    public class AutoMapperModule : Module
    {
        private readonly Assembly[] _assemblies;

        public AutoMapperModule(params Assembly[] assemblies)
        {
            _assemblies = assemblies ?? throw new ArgumentNullException(nameof(assemblies));
        }
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.Register(ctx =>
            {
                var profiles = new List<Profile>();

                foreach (var assembly in _assemblies)
                {
                    profiles.AddRange(assembly.DefinedTypes.Where(x => TypeExtensions.IsAssignableTo<Profile>(x)).Select(x => Activator.CreateInstance(x) as Profile));
                }

                var mapperConfig = new MapperConfiguration(x =>
                {
                    x.DisableConstructorMapping();
                    x.AddProfiles(profiles);
                });

                return mapperConfig.CreateMapper();
            }).As<IMapper>().InstancePerLifetimeScope();
        }
    }
}
