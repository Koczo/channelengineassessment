﻿using Autofac;
using ChannelEngineAssessment.Common.Configurations;
using System;
using Module = Autofac.Module;

namespace ChannelEngineAssessment.Common.IoC
{
    public class ConfigurationModule : Module
    {
        private readonly IApplicationConfiguration _applicationConfiguration;

        public ConfigurationModule(IApplicationConfiguration applicationConfiguration)
        {
            _applicationConfiguration = applicationConfiguration ?? throw new ArgumentNullException(nameof(applicationConfiguration));
        }

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.Register(ctx => _applicationConfiguration).As<IApplicationConfiguration>().SingleInstance();
        }
    }
}
