﻿using Autofac;
using ChannelEngineAssessment.Common.Infrastructure.Repositories;
using Module = Autofac.Module;

namespace ChannelEngineAssessment.Common.IoC
{
    public class RepositoriesModule<TRepository> : Module //where TRepository : IRepository
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            var repositoryAssembly = typeof(TRepository).Assembly;

            builder.RegisterAssemblyTypes(repositoryAssembly)
                .AsClosedTypesOf(typeof(IGenericRepository<>))
                .InstancePerLifetimeScope();

            builder.RegisterType<GenericRestRepository>().As<IGenericRestRepository>().InstancePerLifetimeScope();
        }
    }
}
