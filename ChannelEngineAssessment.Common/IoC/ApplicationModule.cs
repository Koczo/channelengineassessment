﻿using Autofac;
using ChannelEngineAssessment.Common.Application;
using ChannelEngineAssessment.Common.ReadModels;
using Module = Autofac.Module;

namespace ChannelEngineAssessment.Common.IoC
{
    public class ApplicationModule<TService> : Module where TService : IService
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            var serviceAssembly = typeof(TService).Assembly;

            builder.RegisterAssemblyTypes(serviceAssembly)
                .Where(x => x.IsAssignableTo<IService>())
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(serviceAssembly)
                .Where(x => x.IsAssignableTo<IReadModel>())
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

        }
    }
}
