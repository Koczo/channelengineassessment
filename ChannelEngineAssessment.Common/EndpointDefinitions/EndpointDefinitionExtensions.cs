﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ChannelEngineAssessment.Common.EndpointDefinitions
{
    public static class EndpointDefinitionExtensions
    {
        public static void AddEndpontDefinitions(this IServiceCollection services, params Type[] scanMarkers)
        {
            var endpontDefinitions = GetEndpontDefinitions(scanMarkers);
            foreach (var endpointDefinition in endpontDefinitions)
            {
                endpointDefinition.DefineServices(services);
            }

            services.AddSingleton(endpontDefinitions as IReadOnlyCollection<IEndpointDefinition>);
        }

        public static void UseEndpontDefinitions(this WebApplication application)
        {
            var definitions = application.Services.GetRequiredService<IReadOnlyCollection<IEndpointDefinition>>();

            foreach (var endpointDefinition in definitions)
            {
                endpointDefinition.DefineEndpoints(application);
            }
        }

        private static IEnumerable<IEndpointDefinition> GetEndpontDefinitions(Type[] scanMarkers)
        {
            var endpontDefinitions = new List<IEndpointDefinition>();
            if (scanMarkers is null || !scanMarkers.Any())
            {

                endpontDefinitions.AddRange(Assembly.GetExecutingAssembly()
                        .ExportedTypes
                        .Where(x => typeof(IEndpointDefinition).IsAssignableFrom(x) && !x.IsAbstract)
                        .Select(Activator.CreateInstance).Cast<IEndpointDefinition>());
            }
            else
            {
                foreach (var marker in scanMarkers)
                {
                    endpontDefinitions.AddRange(marker.
                        Assembly
                        .ExportedTypes
                        .Where(x => typeof(IEndpointDefinition).IsAssignableFrom(x) && !x.IsAbstract)
                        .Select(Activator.CreateInstance).Cast<IEndpointDefinition>());
                }
            }
            return endpontDefinitions;
        }
    }
}
