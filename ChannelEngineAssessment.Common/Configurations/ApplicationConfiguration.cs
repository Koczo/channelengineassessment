﻿namespace ChannelEngineAssessment.Common.Configurations
{
    public class ApplicationConfiguration : IApplicationConfiguration
    {
        public string BaseApiUrl { get; set; }
        public bool ThrowOnApiError { get; set; }
        public bool FailOnApiDeserializationError { get; set; }
        public string ApiKeyNameUrlParameter { get; set; }
        public string ApiKey { get; set; }
        public IChannelEngineEndpointsConfiguration ChannelEngineEndpoints { get; set; }

        public ApplicationConfiguration()
        {

        }

        public ApplicationConfiguration(string baseApiUrl, bool throwOnApiError, bool failOnApiDeserializationError, 
            string apiKeyNameUrlParameter, string apiKey, IChannelEngineEndpointsConfiguration channelEngineEndpoints)
        {
            BaseApiUrl = baseApiUrl;
            ThrowOnApiError = throwOnApiError;
            FailOnApiDeserializationError = failOnApiDeserializationError;
            ApiKeyNameUrlParameter = apiKeyNameUrlParameter;
            ApiKey = apiKey;
            ChannelEngineEndpoints = channelEngineEndpoints;
        }
    }
}
