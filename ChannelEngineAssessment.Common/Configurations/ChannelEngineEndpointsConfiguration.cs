﻿namespace ChannelEngineAssessment.Common.Configurations
{
    public class ChannelEngineEndpointsConfiguration : IChannelEngineEndpointsConfiguration
    {
        public string GetOrdersEndpoint { get; set; }
        public string UpsertProductsEndpoint { get; set; }
        public string GetProductsEndpoint { get; set; }
        public string GetProductEndpoint { get; set; }

        public ChannelEngineEndpointsConfiguration()
        {

        }

        public ChannelEngineEndpointsConfiguration(string getOrdersEndpoint, string upsertProductsEndpoint,
            string getProductsEndpoint, string getProductEndpoint)
        {
            GetOrdersEndpoint = getOrdersEndpoint;
            UpsertProductsEndpoint = upsertProductsEndpoint;
            GetProductsEndpoint = getProductsEndpoint;
            GetProductEndpoint = getProductEndpoint;
        }

    }
}
