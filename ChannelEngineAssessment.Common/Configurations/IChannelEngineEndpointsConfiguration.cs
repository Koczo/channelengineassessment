﻿namespace ChannelEngineAssessment.Common.Configurations
{
    public interface IChannelEngineEndpointsConfiguration
    {
        string GetOrdersEndpoint { get; }
        string GetProductsEndpoint { get; }
        string GetProductEndpoint { get; }
        string UpsertProductsEndpoint { get; }
    }
}
