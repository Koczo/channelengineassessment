﻿using System.Collections.Generic;

namespace ChannelEngineAssessment.Common.Dtos
{
    public class ListResultDto<TDto>
    {
        public IEnumerable<TDto> Items { get; }
        public ListResultDto(IEnumerable<TDto> items)
        {
            Items = items;
        }
    }
}
