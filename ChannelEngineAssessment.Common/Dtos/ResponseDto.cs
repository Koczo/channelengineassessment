﻿namespace ChannelEngineAssessment.Common.Dtos
{
    public class ResponseDto<TDto>
    {
        public TDto Content { get; set; }
    }
}
