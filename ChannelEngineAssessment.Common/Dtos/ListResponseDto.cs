﻿using System.Collections.Generic;

namespace ChannelEngineAssessment.Common.Dtos
{
    public class ListResponseDto<TDto>
    {
        public IEnumerable<TDto> Content { get; set; }
    }
}
