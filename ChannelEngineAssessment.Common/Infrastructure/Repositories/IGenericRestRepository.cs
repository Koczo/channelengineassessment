﻿using System.Threading.Tasks;
using RestSharp;

namespace ChannelEngineAssessment.Common.Infrastructure.Repositories
{
    public interface IGenericRestRepository
    {
        Task<T> GetAsync<T>(RestRequest restRequest);
        Task<T> PostAsync<T>(RestRequest restRequest);
        Task<T> PutAsync<T>(RestRequest restRequest);
        Task<T> PatchAsync<T>(RestRequest restRequest);
        Task<T> DeleteAsync<T>(RestRequest restRequest);
    }
}