﻿using System;
using System.Net;
using System.Threading.Tasks;
using ChannelEngineAssessment.Common.Configurations;
using ChannelEngineAssessment.Common.Exceptions;
using ChannelEngineAssessment.Common.Infrastructure.Factories;
using RestSharp;

namespace ChannelEngineAssessment.Common.Infrastructure.Repositories
{
    public class GenericRestRepository : IGenericRestRepository
    {
        private readonly IRestClientFactory _restClientFactory;
        private readonly IApplicationConfiguration _applicationConfiguration;

        public GenericRestRepository(IRestClientFactory restClientFactory, IApplicationConfiguration applicationConfiguration)
        {
            _restClientFactory = restClientFactory ?? throw new ArgumentNullException(nameof(restClientFactory));
            _applicationConfiguration = applicationConfiguration ?? throw new ArgumentNullException(nameof(applicationConfiguration));
        }

        public  Task<T> GetAsync<T>(RestRequest restRequest) =>  CallAsync<T>(restRequest, Method.Get);

        public  Task<T> PostAsync<T>(RestRequest restRequest) =>  CallAsync<T>(restRequest, Method.Post);

        public  Task<T> PutAsync<T>(RestRequest restRequest) =>  CallAsync<T>(restRequest, Method.Put);

        public  Task<T> PatchAsync<T>(RestRequest restRequest) =>  CallAsync<T>(restRequest, Method.Patch);

        public  Task<T> DeleteAsync<T>(RestRequest restRequest) =>  CallAsync<T>(restRequest, Method.Delete);

        private async Task<T> CallAsync<T>(RestRequest restRequest, Method method)
        {
            try
            {
                var client = _restClientFactory.Create();
                restRequest.AddParameter(_applicationConfiguration.ApiKeyNameUrlParameter, _applicationConfiguration.ApiKey, ParameterType.QueryString);
                var responseMessage = await client.ExecuteAsync<T>(restRequest, method);

                if (responseMessage.IsSuccessful)
                {
                    return responseMessage.Data;
                }

                if (responseMessage.StatusCode == HttpStatusCode.Forbidden ||
                    responseMessage.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new ApiAuthenticationException(responseMessage.ErrorMessage);
                }

                throw new RestClientUnhandledException(responseMessage.ErrorMessage, responseMessage.StatusCode, responseMessage.ErrorException);

            }
            catch (Exception e)
            {
                if (e is RestClientUnhandledException ex && ex.StatusCode.Equals(HttpStatusCode.NotFound))
                {
                    return default(T);
                }

                throw;
            }
        }
    }
}