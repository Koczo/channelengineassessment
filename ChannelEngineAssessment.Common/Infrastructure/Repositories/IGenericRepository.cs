﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ChannelEngineAssessment.Common.Domain;

namespace ChannelEngineAssessment.Common.Infrastructure.Repositories
{
    public interface IGenericRepository<TAggregate> : IRepository where TAggregate : AggregateRoot
    {
        Task<TAggregate> GetAsync(AggregateId id);
        Task<IEnumerable<TAggregate>> GetAllAsync();
    }
}