﻿using RestSharp;
using System;
using RestSharp.Serializers.NewtonsoftJson;
using ChannelEngineAssessment.Common.Configurations;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;

namespace ChannelEngineAssessment.Common.Infrastructure.Factories
{
    public class RestClientFactory : IRestClientFactory
    {
        private readonly IApplicationConfiguration _applicationConfiguration;

        public RestClientFactory(IApplicationConfiguration applicationConfiguration)
        {
            _applicationConfiguration = applicationConfiguration ?? throw new ArgumentNullException(nameof(applicationConfiguration));
        }


        public RestClient Create()
        {
            var restClientOptions = new RestClientOptions
            {
                BaseUrl = new Uri(_applicationConfiguration.BaseApiUrl),
                ThrowOnAnyError = _applicationConfiguration.ThrowOnApiError,
                FailOnDeserializationError = _applicationConfiguration.FailOnApiDeserializationError
            };

            var client = new RestClient(restClientOptions);

            client.UseNewtonsoftJson(new JsonSerializerSettings
            {
                Converters = { new StringEnumConverter(typeof(SnakeCaseNamingStrategy)) }
            });

            return client;
        }
    }
}
