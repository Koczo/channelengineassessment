﻿using RestSharp;

namespace ChannelEngineAssessment.Common.Infrastructure.Factories
{
    public interface IRestClientFactory
    {
        RestClient Create();
    }
}
