﻿using System.Collections.Generic;

namespace ChannelEngineAssessment.Common.ViewModels
{
    public class ListResultViewModel<TViewModel>
    {
        public IEnumerable<TViewModel> Items { get; set; }
    }
}
