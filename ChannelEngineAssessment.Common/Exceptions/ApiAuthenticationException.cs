﻿using System;

namespace ChannelEngineAssessment.Common.Exceptions
{
    public class ApiAuthenticationException : Exception
    {
        public ApiAuthenticationException(string message) : base(message)
        {
        }
    }
}
