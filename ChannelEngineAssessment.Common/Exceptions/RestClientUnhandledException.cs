﻿using System;
using System.Net;

namespace ChannelEngineAssessment.Common.Exceptions
{
    public class RestClientUnhandledException : Exception
    {
        public HttpStatusCode StatusCode { get; }
        public RestClientUnhandledException(string message, HttpStatusCode statusCode,Exception innerException) : base(message, innerException)
        {
            StatusCode = statusCode;
        }
    }
}
