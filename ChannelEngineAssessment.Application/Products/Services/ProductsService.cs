﻿using ChannelEngineAssessment.Common.Domain;
using ChannelEngineAssessment.Common.Exceptions;
using ChannelEngineAssessment.Domain.Products.Factories;
using ChannelEngineAssessment.Domain.Products.Model;
using ChannelEngineAssessment.Domain.Products.Resources;
using ChannelEngineAssessment.Infrastructure.Products.Repositories;
using System;
using System.Threading.Tasks;

namespace ChannelEngineAssessment.Application.Products.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;
        private readonly IProductDomainFactory _productDomainFactory;

        public ProductService(IProductRepository productRepository, IProductDomainFactory productDomainFactory)
        {
            _productRepository = productRepository ?? throw new ArgumentNullException(nameof(productRepository));
            _productDomainFactory = productDomainFactory ?? throw new ArgumentNullException(nameof(productDomainFactory));
        }

        public async Task SetStockAsync(AggregateId id, int stock)
        {
            var product = await GetProductOrThrow(id);
            product.SetStock(stock);

            await _productRepository.UpdateAsync(product);
        }

        private async Task<Product> GetProductOrThrow(AggregateId id)
        {
            var product = await _productRepository.GetAsync(id);
            if (product == null)
            {
                throw new BusinessLogicException(ProductResources.ProductNotFoundMessage);
            }

            return product;
        }
    }
}
