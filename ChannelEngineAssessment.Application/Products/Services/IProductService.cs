﻿using ChannelEngineAssessment.Common.Application;
using ChannelEngineAssessment.Common.Domain;
using System.Threading.Tasks;

namespace ChannelEngineAssessment.Application.Products.Services
{
    public interface IProductService : IService
    {
        //Task CreateAsync(ProductDataStructure productDataStructure);
        //Task UpdateAsync(ProductDataStructure productDataStructure);
        Task SetStockAsync(AggregateId id, int newStock);
    }
}
