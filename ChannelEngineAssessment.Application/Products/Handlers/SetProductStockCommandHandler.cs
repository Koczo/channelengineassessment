﻿using ChannelEngineAssessment.Application.Products.Services;
using ChannelEngineAssessment.Infrastructure.Products.Commands;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ChannelEngineAssessment.Application.Products.Handlers
{
    public class SetProductStockCommandHandler : IRequestHandler<SetProductStockCommand>
    {
        private readonly IProductService _productService;
        public SetProductStockCommandHandler(IProductService productService)
        {
            _productService = productService ?? throw new ArgumentNullException(nameof(productService));
        }

        public async Task<Unit> Handle(SetProductStockCommand request, CancellationToken cancellationToken)
        {
            await _productService.SetStockAsync(request.MerchantProductNo, request.Stock);
            return Unit.Value;
        }
    }
}
