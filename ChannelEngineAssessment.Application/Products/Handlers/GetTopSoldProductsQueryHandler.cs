﻿using ChannelEngineAssessment.Application.Products.ReadModels;
using ChannelEngineAssessment.Common.Dtos;
using ChannelEngineAssessment.Infrastructure.Products.Dtos;
using ChannelEngineAssessment.Infrastructure.Products.Queries;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ChannelEngineAssessment.Application.Products.Handlers
{
    public class GetTopSoldProductsQueryHandler : IRequestHandler<GetTopSoldProductsQuery, ListResultDto<TopSoldProductDto>>
    {
        private readonly IProductReadModel _productReadModel;
        public GetTopSoldProductsQueryHandler(IProductReadModel productReadModel)
        {
            _productReadModel = productReadModel ?? throw new ArgumentNullException(nameof(_productReadModel));
        }

        public async Task<ListResultDto<TopSoldProductDto>> Handle(GetTopSoldProductsQuery request, CancellationToken cancellationToken)
        {
           return await _productReadModel.GetTopSoldProducts(request);
        }
    }
}
