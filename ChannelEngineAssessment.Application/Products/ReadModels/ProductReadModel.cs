﻿using AutoMapper;
using ChannelEngineAssessment.Common.Domain;
using ChannelEngineAssessment.Common.Dtos;
using ChannelEngineAssessment.Domain.Orders.Model;
using ChannelEngineAssessment.Infrastructure.Orders.Repositories;
using ChannelEngineAssessment.Infrastructure.Products.Dtos;
using ChannelEngineAssessment.Infrastructure.Products.Queries;
using ChannelEngineAssessment.Infrastructure.Products.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelEngineAssessment.Application.Products.ReadModels
{
    public class ProductReadModel : IProductReadModel
    {
        private readonly IProductRepository _productRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;

        public ProductReadModel(IProductRepository productRepository, IOrderRepository orderRepository, IMapper mapper)
        {
            _productRepository = productRepository ?? throw new ArgumentNullException(nameof(productRepository));
            _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<ListResultDto<TopSoldProductDto>> GetTopSoldProducts(GetTopSoldProductsQuery query)
        {
            var orders = await _orderRepository.GetByStatusesAsync(query.OrderStatuses.Select(s => Enumeration.FromValue<OrderStatus>((int)s)));
            var ordersProductNos = orders.SelectMany(x => x.Lines).Select(x => x.MerchantProductNo).Distinct().ToList();

            var productsQuantities = new Dictionary<AggregateId, int>();
            foreach (var ordersProductNo in ordersProductNos)
            {
                var lines = orders.SelectMany(x => x.Lines).Where(x => x.MerchantProductNo.Equals(ordersProductNo));
                productsQuantities.Add(ordersProductNo, lines.Sum(x => x.Quantity));
            }

            var products = await _productRepository.GetByMerchantProductNoAsync(ordersProductNos.Select(x => x.Id));

            return new ListResultDto<TopSoldProductDto>(products.Select(p => new TopSoldProductDto()
            {
                MerchantProductNo = p.Id,
                EAN = p.Ean,
                Name = p.Name,
                TotalQuantity = productsQuantities[p.Id]
            }).OrderByDescending(x => x.TotalQuantity).Take(query.NumberOfProducts));
        }
    }
}
