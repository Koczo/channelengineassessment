﻿using ChannelEngineAssessment.Common.Dtos;
using ChannelEngineAssessment.Common.ReadModels;
using ChannelEngineAssessment.Infrastructure.Products.Dtos;
using ChannelEngineAssessment.Infrastructure.Products.Queries;
using System.Threading.Tasks;

namespace ChannelEngineAssessment.Application.Products.ReadModels
{
    public interface IProductReadModel : IReadModel
    {
        Task<ListResultDto<TopSoldProductDto>> GetTopSoldProducts(GetTopSoldProductsQuery query);
    }
}
