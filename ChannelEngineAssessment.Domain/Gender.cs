﻿using ChannelEngineAssessment.Common.Domain;

namespace ChannelEngineAssessment.Domain
{
    public class Gender : Enumeration
    {
        public static readonly Gender Male
         = new Gender(Common.Domain.Gender.Male, "MALE");

        public static readonly Gender Female
           = new Gender(Common.Domain.Gender.Female, "FEMALE");

        public static readonly Gender NotApplicable
           = new Gender(Common.Domain.Gender.NotApplicable, "NOT_APPLICABLE");

        private Gender(Common.Domain.Gender value, string displayName)
           : base((int)value, displayName)
        {
        }
    }
}
