﻿using ChannelEngineAssessment.Common.Domain;

namespace ChannelEngineAssessment.Domain.Products.Model
{
    public class ProductExtraDataType : Enumeration
    {

        public static readonly ProductExtraDataType Text
            = new ProductExtraDataType(Common.Domain.Products.Enums.ProductExtraDataType.Text, "TEXT");

        public static readonly ProductExtraDataType Number
            = new ProductExtraDataType(Common.Domain.Products.Enums.ProductExtraDataType.Number, "NUMBER");

        public static readonly ProductExtraDataType Url
            = new ProductExtraDataType(Common.Domain.Products.Enums.ProductExtraDataType.Url, "URL");

        public static readonly ProductExtraDataType ImageUrl
            = new ProductExtraDataType(Common.Domain.Products.Enums.ProductExtraDataType.ImageUrl, "IMAGEURL");


        private ProductExtraDataType(Common.Domain.Products.Enums.ProductExtraDataType value, string displayName)
           : base((int)value, displayName)
        {
        }
    }
    
}
