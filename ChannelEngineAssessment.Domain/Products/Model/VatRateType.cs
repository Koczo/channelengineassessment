﻿using ChannelEngineAssessment.Common.Domain;

namespace ChannelEngineAssessment.Domain.Products.Model
{
    public class VatRateType : Enumeration
    {
        public static readonly VatRateType None
            = new VatRateType(Common.Domain.Products.Enums.VatRateType.None, "NONE");

        public static readonly VatRateType Standard
            = new VatRateType(Common.Domain.Products.Enums.VatRateType.Standard, "STANDARD");

        public static readonly VatRateType Reduced
            = new VatRateType(Common.Domain.Products.Enums.VatRateType.Reduced, "REDUCED");

        public static readonly VatRateType SuperReduced
            = new VatRateType(Common.Domain.Products.Enums.VatRateType.SuperReduced, "SUPER_REDUCED");

        public static readonly VatRateType Exempt
            = new VatRateType(Common.Domain.Products.Enums.VatRateType.Exempt, "EXEMPT");


        private VatRateType(Common.Domain.Products.Enums.VatRateType value, string displayName)
           : base((int)value, displayName)
        {
        }
    }
    
}
