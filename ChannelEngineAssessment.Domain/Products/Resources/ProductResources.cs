﻿namespace ChannelEngineAssessment.Domain.Products.Resources
{
    public static class ProductResources
    {
        public const string StockHasToBeZeroOrPositiveMessage = nameof(StockHasToBeZeroOrPositiveMessage);
        public const string ProductAlreadyExistsMessage = nameof(ProductAlreadyExistsMessage);
        public const string ProductNotFoundMessage = nameof(ProductNotFoundMessage);
        public const string TooManyImageUrlsMessage = nameof(TooManyImageUrlsMessage);
        public const string WrongImageUrlMessage = nameof(WrongImageUrlMessage);
        public const string ExternalImageUrlNotFoundMessage = nameof(ExternalImageUrlNotFoundMessage);
        public const string NameCannotBeEmptyMessage = nameof(NameCannotBeEmptyMessage);
    }
}
