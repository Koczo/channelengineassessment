﻿using ChannelEngineAssessment.Common.Domain;
using ChannelEngineAssessment.Domain.Products.DataStructures;
using ChannelEngineAssessment.Domain.Products.Model;

namespace ChannelEngineAssessment.Domain.Products.Factories
{
    public interface IProductDomainFactory : IDomainFactory
    {
        Product Create(ProductDataStructure dataStructure);
    }
}
