﻿using ChannelEngineAssessment.Domain.Products.DataStructures;
using ChannelEngineAssessment.Domain.Products.Model;

namespace ChannelEngineAssessment.Domain.Products.Factories
{
    public class ProductDomainFactory : IProductDomainFactory
    {
        public Product Create(ProductDataStructure dataStructure)
        {
            var product = new Product(dataStructure.MerchantProductNo, dataStructure);
            product.AddExtraImageUrls(dataStructure.ExtraImageUrls);

            return product;
        }
    }
}
