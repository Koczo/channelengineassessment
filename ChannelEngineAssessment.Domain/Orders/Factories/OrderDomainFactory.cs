﻿using ChannelEngineAssessment.Domain.Orders.DataStructures;
using ChannelEngineAssessment.Domain.Orders.Model;

namespace ChannelEngineAssessment.Domain.Orders.Factories
{
    public class OrderDomainFactory : IOrderDomainFactory
    {
        public Order Create(OrderDataStructure dataStructure)
        {
            return new Order(dataStructure.ChannelOrderNo, dataStructure);
        }
    }
}
