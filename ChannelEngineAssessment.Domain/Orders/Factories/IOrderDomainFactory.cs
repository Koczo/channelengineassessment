﻿using ChannelEngineAssessment.Common.Domain;
using ChannelEngineAssessment.Domain.Orders.DataStructures;
using ChannelEngineAssessment.Domain.Orders.Model;

namespace ChannelEngineAssessment.Domain.Orders.Factories
{
    public interface IOrderDomainFactory : IDomainFactory
    {
        Order Create(OrderDataStructure dataStructure);
    }
}
