﻿using ChannelEngineAssessment.Common.Domain;

namespace ChannelEngineAssessment.Domain.Orders.Model
{
    public class OrderSupport : Enumeration
    {

        public static readonly OrderSupport None
            = new OrderSupport(Common.Domain.Orders.Enums.OrderSupport.None, "NONE");

        public static readonly OrderSupport Orders
            = new OrderSupport(Common.Domain.Orders.Enums.OrderSupport.Orders, "ORDERS");

        public static readonly OrderSupport SplitOrders
            = new OrderSupport(Common.Domain.Orders.Enums.OrderSupport.SplitOrders, "SPLIT_ORDERS");

        public static readonly OrderSupport SplitOrderLines
            = new OrderSupport(Common.Domain.Orders.Enums.OrderSupport.SplitOrderLines, "SPLIT_ORDER_LINES");


        private OrderSupport(Common.Domain.Orders.Enums.OrderSupport value, string displayName)
           : base((int)value, displayName)
        {
        }
    }
}
