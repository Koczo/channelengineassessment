﻿using ChannelEngineAssessment.Common.Domain;
using ChannelEngineAssessment.Domain.Orders.DataStructures;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ChannelEngineAssessment.Domain.Orders.Model
{
    public class Order : AggregateRoot
    {
        public int ChannelId { get; private set; }
        public string ChannelName { get; private set; }
        public int GlobalChannelId { get; private set; }
        public string GlobalChannelName { get; private set; }
        public OrderSupport ChannelOrderSupport { get; private set; }
        public OrderStatus Status { get; private set; }
        public bool IsBusinessOrder { get; private set; }
        public string MerchantComment { get; private set; }
        public Address BillingAddress { get; private set; }
        public Address ShippingAddress { get; private set; }
        public PriceWithVat? SubTotalPriceWithVat { get; private set; }
        public PriceWithVat? TotalPriceWithVat { get; private set; }
        public PriceWithVat? ShippingPriceWithVat { get; private set; }
        public PriceWithVat? OriginalSubtotalPriceWithVat { get; private set; }
        public PriceWithVat? OriginalTotalPriceWithVat { get; private set; }
        public PriceWithVat OriginalShippingPriceWithVat { get; private set; }
        public string Phone { get; private set; }
        public string Email { get; private set; }
        public string CompanyRegistrationNo { get; private set; }
        public string VatNo { get; private set; }
        public string PaymentMethod { get; private set; }
        public string CurrencyCode { get; private set; }
        public DateTime OrderDate { get; private set; }
        public string ChannelCustomerNo { get; private set; }
        public Dictionary<string, string> ExtraData { get; private set; }

        private List<OrderLine> _lines = new List<OrderLine>();
        public IEnumerable<OrderLine> Lines
        {
            get => _lines;
            private set => _lines = value.ToList();
        }


        internal Order(AggregateId id, OrderDataStructure orderDataStructure) : base(id)
        {
            AssignDataStructure(orderDataStructure);
        }

        private Order()
        {

        }

        private void AssignDataStructure(OrderDataStructure orderDataStructure)
        {
            ChannelId = orderDataStructure.ChannelId;
            ChannelName = orderDataStructure.ChannelName;
            GlobalChannelId = orderDataStructure.GlobalChannelId;
            GlobalChannelName = orderDataStructure.GlobalChannelName;
            ChannelOrderSupport = orderDataStructure.ChannelOrderSupport;
            Status = orderDataStructure.Status;
            IsBusinessOrder = orderDataStructure.IsBusinessOrder;
            MerchantComment = orderDataStructure.MerchantComment;
            BillingAddress = orderDataStructure.BillingAddress;
            ShippingAddress = orderDataStructure.ShippingAddress;
            SubTotalPriceWithVat = orderDataStructure.SubTotalPriceWithVat;
            TotalPriceWithVat = orderDataStructure.TotalPriceWithVat;
            ShippingPriceWithVat = orderDataStructure.ShippingPriceWithVat;
            OriginalSubtotalPriceWithVat = orderDataStructure.OriginalSubtotalPriceWithVat;
            OriginalTotalPriceWithVat = orderDataStructure.OriginalTotalPriceWithVat;
            OriginalShippingPriceWithVat = orderDataStructure.OriginalShippingPriceWithVat;
            Phone = orderDataStructure.Phone;
            Email = orderDataStructure.Email;
            CompanyRegistrationNo = orderDataStructure.CompanyRegistrationNo;
            VatNo = orderDataStructure.VatNo;
            PaymentMethod = orderDataStructure.PaymentMethod;
            CurrencyCode = orderDataStructure.CurrencyCode;
            OrderDate = orderDataStructure.OrderDate;
            ChannelCustomerNo = orderDataStructure.ChannelCustomerNo;
            ExtraData = orderDataStructure.ExtraData;

            foreach (var lineDataStructure in orderDataStructure.Lines)
            {
                _lines.Add(new OrderLine(lineDataStructure));
            }
        }
    }
}