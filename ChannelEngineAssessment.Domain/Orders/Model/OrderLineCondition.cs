﻿using ChannelEngineAssessment.Common.Domain;

namespace ChannelEngineAssessment.Domain.Orders.Model
{
    public class OrderLineCondition : Enumeration
    {
        public static readonly OrderLineCondition New
           = new OrderLineCondition(Common.Domain.Orders.Enums.OrderLineCondition.New, "NEW");

        public static readonly OrderLineCondition NewRefurbished
            = new OrderLineCondition(Common.Domain.Orders.Enums.OrderLineCondition.NewRefurbished, "NEW_REFURBISHED");

        public static readonly OrderLineCondition UsedAsNew
            = new OrderLineCondition(Common.Domain.Orders.Enums.OrderLineCondition.UsedAsNew, "USED_AS_NEW");

        public static readonly OrderLineCondition UsedGood
            = new OrderLineCondition(Common.Domain.Orders.Enums.OrderLineCondition.UsedGood, "USED_GOOD");

        public static readonly OrderLineCondition UsedReasonable
            = new OrderLineCondition(Common.Domain.Orders.Enums.OrderLineCondition.UsedReasonable, "USED_REASONABLE");

        public static readonly OrderLineCondition UsedMediocre
            = new OrderLineCondition(Common.Domain.Orders.Enums.OrderLineCondition.UsedMediocre, "USED_MEDIOCRE");

        public static readonly OrderLineCondition Unknown
           = new OrderLineCondition(Common.Domain.Orders.Enums.OrderLineCondition.Unknown, "UNKNOWN");

        public static readonly OrderLineCondition UsedVeryGood
           = new OrderLineCondition(Common.Domain.Orders.Enums.OrderLineCondition.UsedVeryGood, "USED_VERY_GOOD");

        private OrderLineCondition(Common.Domain.Orders.Enums.OrderLineCondition value, string displayName)
           : base((int)value, displayName)
        {
        }
    }
}
