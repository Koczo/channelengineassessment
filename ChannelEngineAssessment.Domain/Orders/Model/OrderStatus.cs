﻿using ChannelEngineAssessment.Common.Domain;

namespace ChannelEngineAssessment.Domain.Orders.Model
{
    public class OrderStatus : Enumeration
    {

        public static readonly OrderStatus New
            = new OrderStatus(Common.Domain.Orders.Enums.OrderStatus.New, "NEW");

        public static readonly OrderStatus InProgress
            = new OrderStatus(Common.Domain.Orders.Enums.OrderStatus.InProgress, "IN_PROGRESS");

        public static readonly OrderStatus Shipped
            = new OrderStatus(Common.Domain.Orders.Enums.OrderStatus.Shipped, "SHIPPED");

        public static readonly OrderStatus Manco
            = new OrderStatus(Common.Domain.Orders.Enums.OrderStatus.Manco, "MANCO");

        public static readonly OrderStatus InBackorder
            = new OrderStatus(Common.Domain.Orders.Enums.OrderStatus.InBackorder, "IN_BACKORDER");

        public static readonly OrderStatus InCombi
            = new OrderStatus(Common.Domain.Orders.Enums.OrderStatus.InCombi, "IN_COMBI");

        public static readonly OrderStatus RequiresCorrection
           = new OrderStatus(Common.Domain.Orders.Enums.OrderStatus.RequiresCorrection, "REQUIRES_CORRECTION");

        public static readonly OrderStatus Returned
           = new OrderStatus(Common.Domain.Orders.Enums.OrderStatus.Returned, "RETURNED");

        public static readonly OrderStatus Closed
           = new OrderStatus(Common.Domain.Orders.Enums.OrderStatus.Closed, "CLOSED");

        public static readonly OrderStatus Canceled
           = new OrderStatus(Common.Domain.Orders.Enums.OrderStatus.Canceled, "CANCELED");

        private OrderStatus(Common.Domain.Orders.Enums.OrderStatus value, string displayName)
           : base((int)value, displayName)
        {
        }
    }
}
