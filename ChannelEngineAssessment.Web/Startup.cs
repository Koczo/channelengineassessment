using Autofac;
using ChannelEngineAssessment.Application.Products.Handlers;
using ChannelEngineAssessment.Application.Products.Services;
using ChannelEngineAssessment.Common.Configurations;
using ChannelEngineAssessment.Common.IoC;
using ChannelEngineAssessment.Domain.Orders.Factories;
using ChannelEngineAssessment.Infrastructure.Orders.Repositories;
using ChannelEngineAssessment.Web.Middlewares;
using ChannelEngineAssessment.Web.ViewModels.MappingProfiles;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace ChannelEngineAssessment.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc("v1", new OpenApiInfo
            //    {
            //        Title = "Zomato API",
            //        Version = "v1",
            //        Description = "Description for the API goes here.",
            //        Contact = new OpenApiContact
            //        {
            //            Name = "Ankush Jain",
            //            Email = string.Empty,
            //            Url = new Uri("https://coderjony.com/"),
            //        },
            //    });
            //});
            services.AddControllersWithViews();
            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            var channelEngineEndpointsConfiguration = new ChannelEngineEndpointsConfiguration();
            Configuration.GetSection("ApplicationConfiguration:ChannelEngineEndpoints").Bind(channelEngineEndpointsConfiguration);
            builder.RegisterModule(new ConfigurationModule(
                new ApplicationConfiguration(
                    Configuration["ApplicationConfiguration:BaseApiUrl"],
                    Convert.ToBoolean(Configuration["ApplicationConfiguration:ThrowOnApiError"]),
                    Convert.ToBoolean(Configuration["ApplicationConfiguration:FailOnApiDeserializationError"]),
                    Configuration["ApplicationConfiguration:ApiKeyNameUrlParameter"],
                    Configuration["ApplicationConfiguration:ApiKey"],
                    channelEngineEndpoints: channelEngineEndpointsConfiguration)));

            builder.RegisterModule(new ApplicationModule<ProductService>());
            builder.RegisterModule(new RepositoriesModule<OrderRepository>());
            builder.RegisterModule(new FactoriesModule<OrderDomainFactory>());
            builder.RegisterModule(new MediatorModule(typeof(GetTopSoldProductsQueryHandler).Assembly));
            builder.RegisterModule(new AutoMapperModule(typeof(ProductsControllerMappingProfile).Assembly, typeof(OrderRepository).Assembly));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseMiddleware<RequestLoggingMiddleware>();
            app.UseMiddleware<ApplicationExceptionMiddleware>();

            //app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Zomato API V1");

            //    // To serve SwaggerUI at application's root page, set the RoutePrefix property to an empty string.
            //    c.RoutePrefix = string.Empty;
            //});

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }
    }
}
