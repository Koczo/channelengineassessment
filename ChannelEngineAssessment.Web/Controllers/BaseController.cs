﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;

namespace ChannelEngineAssessment.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BaseController : ControllerBase
    {
        protected readonly IMediator Mediator;
        protected readonly IMapper Mapper;
        public BaseController(IMediator mediator, IMapper mapper)
        {
            Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            Mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
    }
}
