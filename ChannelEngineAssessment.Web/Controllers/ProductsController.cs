﻿using AutoMapper;
using ChannelEngineAssessment.Common.Domain;
using ChannelEngineAssessment.Common.ViewModels;
using ChannelEngineAssessment.Infrastructure.Products.Commands;
using ChannelEngineAssessment.Infrastructure.Products.Queries;
using ChannelEngineAssessment.Web.ViewModels.Products;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ChannelEngineAssessment.Web.Controllers
{
   
    public class ProductsController : BaseController
    {
        public ProductsController(IMediator mediator, IMapper mapper) : base(mediator, mapper)
        {
        }

        [HttpGet("top-sold")]
        public async Task<IActionResult> GetTopSoldProductsAsync()
        {
            var query = new GetTopSoldProductsQuery();

            var result = await Mediator.Send(query);

            return Ok(Mapper.Map<ListResultViewModel<TopSoldProductViewModel>>(result));
        }

        [HttpPatch("set-stock")]
        public async Task<IActionResult> SetProductStock([FromBody] SetProductStockViewModel viewModel)
        {
            var command = new SetProductStockCommand(new AggregateId(viewModel.MerchantProductNo), viewModel.Stock);

            await Mediator.Send(command);

            return Ok();
        }
    }
}
