import { Component, Input, OnInit } from '@angular/core';
import { ProductsService } from './services/products.service';
import { TopSoldProductModel } from './models/top-sold-product.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  public topSoldProducts : TopSoldProductModel[];
  public isSetStockModalVisible: boolean = false;
  public merchantProductNo: string;

  public constructor(private _productsService: ProductsService, private _router: Router) { }

  public ngOnInit(): void {
    this.getProducts();
  }

  public getProducts(): void {
    this._productsService.getTopSoldProducts().subscribe(x => this.topSoldProducts = x.items);
  }

  public openProductDetails(merchantProductNo: string): void {
    this.merchantProductNo = merchantProductNo;
    this.isSetStockModalVisible = true;
  }

  public setNewStock(newStock: number): void {
    this._productsService.setNewStock(this.merchantProductNo , newStock).subscribe(() => this.getProducts());
    this.isSetStockModalVisible = false;
  }

}
