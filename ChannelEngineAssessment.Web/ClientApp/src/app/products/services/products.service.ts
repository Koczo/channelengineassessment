import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TopSoldProductModel } from '../models/top-sold-product.model';
import { ListResultModel } from '../models/list-result.model';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  public constructor(private _http: HttpClient, @Inject('BASE_URL')  private _baseUrl: string) { }

  public getTopSoldProducts(): Observable<ListResultModel<TopSoldProductModel>> {
    return this._http.get<ListResultModel<TopSoldProductModel>>(this._baseUrl + 'products/top-sold');
  }

  public setNewStock(merchantProductNo: string, newStock: number): Observable<void> {
    return this._http.patch<void>(this._baseUrl + 'products/set-stock', {merchantProductNo, newStock});
  }


}
