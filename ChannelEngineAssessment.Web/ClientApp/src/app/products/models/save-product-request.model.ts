
export type SaveProductRequestModel = {
    merchantProductNo: string;
    isActive: boolean;
    name: string;
    description: string;
    brand: string;
    size: string;
    color: string;
    ean: string;
    manufacturerProductNumber: string;
    price: number;
    mSRP: number;
    purchasePrice: number;
    shippingCost: number;
    shippingTime: string;
    url: string;
    imageUrl: string;
    extraImageUrls: string[];
    categoryTrail: string;
}