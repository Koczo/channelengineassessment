export class TopSoldProductModel {
    public merchantProductNo: string;
    public name: string;
    public ean: string;
    public totalQuantity: number;
}