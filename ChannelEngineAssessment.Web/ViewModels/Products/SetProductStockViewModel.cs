﻿namespace ChannelEngineAssessment.Web.ViewModels.Products
{
    public class SetProductStockViewModel
    {
        public string MerchantProductNo { get; set; }
        public int Stock { get; set; }
    }
}
