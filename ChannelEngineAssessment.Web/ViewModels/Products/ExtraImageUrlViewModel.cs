﻿namespace ChannelEngineAssessment.Web.ViewModels.Products
{
    public class ExtraImageUrlViewModel
    {
        public int No { get; set; }
        public string Url { get; set; }
    }
}
