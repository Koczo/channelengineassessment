﻿namespace ChannelEngineAssessment.Web.ViewModels.Products
{
    public enum VatRateType
    {
        Standard = 1,
        Reduced = 2,
        SuperReduced = 3,
        Exempt = 4
    }
}