﻿using AutoMapper;
using ChannelEngineAssessment.Common.Dtos;
using ChannelEngineAssessment.Common.ViewModels;
using ChannelEngineAssessment.Infrastructure.Products.Dtos;
using ChannelEngineAssessment.Web.ViewModels.Products;

namespace ChannelEngineAssessment.Web.ViewModels.MappingProfiles
{
    public class ProductsControllerMappingProfile : Profile
    {
        public ProductsControllerMappingProfile()
        {
            CreateMap<TopSoldProductDto, TopSoldProductViewModel>()
                .ForMember(x => x.MerchantProductNo, opt => opt.MapFrom(x => x.MerchantProductNo.ToString()));

            CreateMap<ListResultDto<TopSoldProductDto>, ListResultViewModel<TopSoldProductViewModel>>()
                .ForMember(x => x.Items, opt => opt.MapFrom(x => x.Items));
        }
    }
}
