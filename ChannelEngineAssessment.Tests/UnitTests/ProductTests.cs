﻿using AutoFixture;
using ChannelEngineAssessment.Common.Domain;
using ChannelEngineAssessment.Common.Exceptions;
using ChannelEngineAssessment.Domain.Products.DataStructures;
using ChannelEngineAssessment.Domain.Products.Factories;
using ChannelEngineAssessment.Domain.Products.Model;
using System;
using System.Collections.Generic;
using Xunit;

namespace ChannelEngineAssessment.Tests.UnitTests
{
    public class ProductTests
    {
        public ProductTests()
        {

        }

        [Fact]
        public void ShouldCreateProduct()
        {
            Fixture fixture = new Fixture();
            var product = CreateProduct(fixture);
            Assert.NotNull(product);
        }

        [Fact]
        public void ShouldSetStock()
        {
            Fixture fixture = new Fixture();
            var product = CreateProduct(fixture);
            product.SetStock(20);

            Assert.Equal(20, product.Stock);
        }

        [Fact]
        public void ShouldThrowWhenSetStockIsNegative()
        {
            Fixture fixture = new Fixture();
            var product = CreateProduct(fixture);
            
            Assert.Throws<BusinessLogicException>(() => product.SetStock(-1));
        }

        [Fact]
        public void ShouldThrowBusinessLogicExceptionWhenNameIsEmptyOnUpdate()
        {
            Fixture fixture = new Fixture();
            var product = CreateProduct(fixture);

            Assert.Throws<BusinessLogicException>(() => product.Update(new ProductDataStructure(
                merchantProductNo: AggregateId.Generate(),
                isActive: true,
                extraData: new List<ProductExtraData>(),
                name: string.Empty,
                description: fixture.Create<string>(),
                brand: fixture.Create<string>(),
                size: fixture.Create<string>(),
                color: fixture.Create<string>(),
                ean: fixture.Create<string>(),
                manufacturerProductNumber: fixture.Create<string>(),
                price: fixture.Create<decimal>(),
                msrp: fixture.Create<decimal>(),
                purchasePrice: fixture.Create<decimal>(),
                vatRateType: VatRateType.Standard,
                shippingCost: fixture.Create<decimal>(),
                shippingTime: fixture.Create<DateTime>().ToString(),
                url: fixture.Create<string>(),
                imageUrl: fixture.Create<string>(),
                categoryTrail: fixture.Create<string>(),
                new List<string>())));
        }


        public Product CreateProduct(Fixture fixture)
            => new ProductDomainFactory().Create(new ProductDataStructure(
                merchantProductNo: AggregateId.Generate(),
                isActive: true,
                extraData: new List<ProductExtraData>(),
                name: fixture.Create<string>(),
                description: fixture.Create<string>(),
                brand: fixture.Create<string>(),
                size: fixture.Create<string>(),
                color: fixture.Create<string>(),
                ean: fixture.Create<string>(),
                manufacturerProductNumber: fixture.Create<string>(),
                price: fixture.Create<decimal>(),
                msrp: fixture.Create<decimal>(),
                purchasePrice: fixture.Create<decimal>(),
                vatRateType: VatRateType.Standard,
                shippingCost: fixture.Create<decimal>(),
                shippingTime: fixture.Create<DateTime>().ToString(),
                url: fixture.Create<string>(),
                imageUrl: fixture.Create<string>(),
                categoryTrail: fixture.Create<string>(),
                new List<string>()));
    }
}
