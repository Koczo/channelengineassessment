﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using ChannelEngineAssessment.Web;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ChannelEngineAssessment.Tests.IntegrationTests
{
    public class BaseIntegrationTests
    {
        protected readonly TestServer Server;
        protected readonly ILifetimeScope Scope;
        protected readonly IMediator Mediator;
        public BaseIntegrationTests()
        {
            Server = new TestServer(new WebHostBuilder()
                .UseConfiguration(new ConfigurationBuilder().AddJsonFile("appsettings.json").Build())
                .ConfigureServices((services => ServiceCollectionExtensions.AddAutofac(services, null)))
                .UseStartup<Startup>());
            Scope = Server.Host.Services.GetService<ILifetimeScope>() ?? throw new ArgumentNullException(nameof(Scope));
            Mediator = Scope.Resolve<IMediator>();
        }
    }
}