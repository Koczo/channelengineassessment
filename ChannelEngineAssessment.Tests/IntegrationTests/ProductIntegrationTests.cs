﻿using System.Collections.Generic;
using System.Linq;
using Autofac;
using ChannelEngineAssessment.Domain.Orders.Model;
using ChannelEngineAssessment.Infrastructure.Orders.Repositories;
using ChannelEngineAssessment.Infrastructure.Products.Queries;

using Xunit;

namespace ChannelEngineAssessment.Tests.IntegrationTests
{
    public class ProductIntegrationTests : BaseIntegrationTests
    {
        public ProductIntegrationTests() : base()
        {
   
        }

        [Fact]
        public void ShouldGetMaximumFiveProductsFromRepository()
        {
            var query = new GetTopSoldProductsQuery(5);
            var dto = Mediator.Send(query).GetAwaiter().GetResult();

            Assert.NotNull(dto);
            Assert.True(dto.Items.Count() <= 5);
        }

        [Fact]
        public void ShouldGetOnlyOrdersInProgressStatus()
        {
            var repository = Scope.Resolve<IOrderRepository>();
            var result = repository.GetByStatusesAsync(new List<OrderStatus>() { OrderStatus.InProgress }).GetAwaiter().GetResult();

            Assert.NotNull(result);
            Assert.All(result, o => o.Status.Equals(OrderStatus.InProgress));
        }
    }
}